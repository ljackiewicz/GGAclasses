#include <iostream>
#include <vector>
#include <algorithm>

// Jackiewicz Łukasz
// Geometria, grafy, algorytmy
// Technika zamiatania: Punkty dominujące
//
// Kompilacja: g++ main.cpp
// Uruchomienie: ./a.out lub a.exe

using namespace std;

class Point {
public:
    int x;
    int y;
};

// wyznaczanie otoczki wypukłej (zamiatanie: lewo, prawo)
void detect_maxima(int n, vector<Point> tab, vector<Point> *result);

// usuwanie powtórzeń po zamiataniu
vector<Point> unique_points(int n, vector<Point> &tab);

// wypełnianie wektora na podstawie dwóch tablic
void fill_vector(int size, const int *tab1, const int *tab2, vector<Point> *vec);

// wyświetlanie wektora punktów
void print_points(int n, vector<Point> tab);

bool sort_x(Point a, Point b) { return a.x < b.x; }

int main(void) {

    vector<Point> points;

    vector<Point> pointsX;
    vector<Point> pointsXrev;

    vector<Point> result;

    int tab_x[] = {1,4,3,4,2,3,5,6};
    int tab_y[] = {5,4,5,6,3,1,3,5};

    int size = sizeof(tab_x)/sizeof(tab_x[0]);

    // wypełnianie wektora punktów
    fill_vector(size, tab_x, tab_y, &points);

    // sortowanie punktów po wartości x (zamiatanie z lewej do prawej)
    pointsX = points;
    sort(pointsX.begin(), pointsX.end(), sort_x);

    // odwracanie tablicy punktów posortowanych po x (zamiatanie z prawej do lewej)
    pointsXrev = pointsX;
    reverse(pointsXrev.begin(), pointsXrev.end());

    // zamiatanie
    detect_maxima(size, pointsX, &result);
    detect_maxima(size, pointsXrev, &result);

    // usuwanie powtórzeń po zamiataniu
    result = unique_points(size, result);

    cout<<"Otoczke wypukla dla zadanego zbioru tworza nastepujace punkty: "<<endl;
    print_points(result.size(), result);

    return 0;
}

void detect_maxima(int n, vector<Point> tab, vector<Point> *result) {

    Point temp = tab[0];
    int y_max = temp.y;
    int y_min = temp.y;

    //cout<<"("<<temp.x<<","<<temp.y<<")"<<endl;
    result->push_back(temp);

    for(int i=1; i<n; i++) {
        if(tab[i].y > y_max) {
            //cout<<"("<<tab[i].x<<","<<tab[i].y<<")"<<endl;
            result->push_back(tab[i]);
            y_max = tab[i].y;
        } else if(tab[i].y < y_min) {
            //cout<<"("<<tab[i].x<<","<<tab[i].y<<")"<<endl;
            result->push_back(tab[i]);
            y_min = tab[i].y;
        }
    }
}

vector<Point> unique_points(int n, vector<Point> &tab) {
    vector<Point> result;
    Point temp1 = {0,0};
    Point temp2 = {0,0};

    sort(tab.begin(), tab.end(), sort_x);

    for(int i=0; i<n; i++) {
        temp1 = tab[i];
        temp2 = tab[i+1];
        if(!((temp1.x == temp2.x) && (temp1.y == temp2.y))) {
            result.push_back(temp1);
        }
    }

    return result;
}

void fill_vector(int size, const int *tab1, const int *tab2, vector<Point> *vec) {
    Point temp = {0,0};

    for(int i=0; i<size; i++) {
        temp.x = tab1[i];
        temp.y = tab2[i];

        vec->push_back(temp);
    }
}

void print_points(int n, vector<Point> tab) {
    for(int i=0; i<n; i++) {
        cout<<"("<<tab[i].x<<","<<tab[i].y<<")\t";
    }
    cout<<endl;
}
