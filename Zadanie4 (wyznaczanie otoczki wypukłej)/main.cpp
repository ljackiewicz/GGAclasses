#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stack>

// Jackiewicz Lukasz
// Geometria, grafy, algorytmy
// Zadanie 4: Wyznaczanie otoczki wypuklej
//
// Kompilacja: g++ main.cpp
// Uruchomienie: ./a.out lub a.exe

using namespace std;

struct Point {
    int x, y;
};

// najniższy punkt
Point p0;

// wyswietlanie zawartosci wektora
void print_points(vector<Point> tab);

// wypelnianie wektora podanymi tablicami
void fill_vector(const int *tab1, const int *tab2, int size, vector<Point> *vec);

// warunek sortowania - najmniejsze wartosci x i y
bool sort_xy(Point a, Point b) { return (a.y < b.y && a.x < b.x); }

// wyznaczanie odleglosci miedzy punktami, bez operacji pierwiastkowania
int dist(Point p1, Point p2) {
    return pow((p2.x - p1.x),2) + pow((p2.y - p1.y),2);
}

// funkcja okreslajaca jak kolejne punkty ulozone sa wzgledem siebie
int orientation(Point p, Point q, Point r) {
    int value = ((q.y - p.y) * (r.x - q.x)) - ((q.x - p.x) * (r.y - q.y));

    if(value == 0) {
        // colinear
        return 0;
    } else if(value > 0) {
        // clockwise
        return 1;
    } else {
        // counterclockwise
        return 2;
    }
}

// funkcja porownujaca sluzaca do sortowania (counterclockwise)
int compare(const void *vp1, const void *vp2) {
    Point *p1 = (Point *)vp1;
    Point *p2 = (Point *)vp2;

    int value = orientation(p0, *p1, *p2);
    if(value == 0) {
        if(dist(p0, *p2) > dist(p0, *p1)) {
            return -1;
        } else {
            return 1;
        }
    } else if(value == 2) {
        return -1;
    } else {
        return 1;
    }
}

void convex_hull(vector<Point> tab) {
    int size = tab.size();
    vector<Point> points;

    // wyznaczanie najniższego punktu
    sort(tab.begin(), tab.end(), sort_xy);
    p0 = tab[0];

    // sortowanie po kacie wzgledem p0
    // w przypadku kilku punktow posiadajacych ten sam kat z p0 najdalszy bedzie umieszczany na koncu
    qsort(&tab[1], size-1, sizeof(Point), compare);

    points.push_back(p0);
    // usuwanie punktow posiadajacych ten sam kat wzgledem p0 (oprocz najdalszego)
    for(int i=1; i<size; i++) {
        while(i < size-1 && orientation(p0, tab[i], tab[i+1]) == 0) {
            i++;
        }
        points.push_back(tab[i]);
    }

    // sprawdzenie ilosci punktow po usunieciu tych lezacych na jednej linii wzdlegem p0
    if(points.size() < 3) {
        cout<<"Nie mozna wyznaczyc otoczki wypuklej dla tego zbioru punktow"<<endl;
        return;
    }

    // operacje na stosie: sprawdzanie kolejnych punkow wzgledem wzajemnego polozenia
    // utworzenie pustego stosu i wrzucenie trzech pierwszych punktow
    stack<Point> hull;
    hull.push(points[0]);
    hull.push(points[1]);
    hull.push(points[2]);

    // sprawdzanie kolejnych punkow wzgledem wzajemnego polozenia
    for(int i=3; i<points.size(); i++) {
        Point top = hull.top();
        hull.pop();

        while(orientation(hull.top(), top, points[i]) != 2) {
            top = hull.top();
            hull.pop();
        }
        hull.push(top);
        hull.push(points[i]);
    }

    // wyswietlanie wyniku
    while(!hull.empty()) {
        Point p = hull.top();
        cout<<"("<<p.x<<","<<p.y<<")\t";
        hull.pop();
    }
    cout<<endl;
}

int main(void) {

    vector<Point> points;

    int tab_x[] = {8,7,1,3,4,12,10,9,6,7,4,5,5,6,4};
    int tab_y[] = {1,2,4,4,3,7,5,7,5,7,6,8,9,3,5};

    int size = sizeof(tab_x)/sizeof(tab_x[0]);
    fill_vector(tab_x, tab_y,size, &points);

    convex_hull(points);

    return 0;
}

void print_points(vector<Point> tab) {
    for(int i=0; i<tab.size(); i++) {
        cout<<"("<<tab[i].x<<","<<tab[i].y<<")\t";
    }
    cout<<endl;
}

void fill_vector(const int *tab1, const int *tab2, int size, vector<Point> *vec) {
    Point temp = {0,0};

    for(int i=0; i<size; i++) {
        temp.x = tab1[i];
        temp.y = tab2[i];

        vec->push_back(temp);
    }
}