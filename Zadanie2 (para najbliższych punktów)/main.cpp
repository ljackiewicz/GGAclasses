#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <cstdlib>

// Jackiewicz Lukasz
// Geometria, grafy, algorytmy
// Zadanie 2: Para najblizszych punktow
//
// Kompilacja: g++ -std=c++1y main.cpp (lub -std=c++14)

using namespace std;

// klasa reprezentujaca punkty
class Point {
    public:
        int x;
        int y;
};

// klasa reprezentujaca pare punktow (odcinek)
class Segment {
    public:
        Point a;
        Point b;
        double dist;
};

// funkcja wypelniajaca wektor na podstawie dwoch tablic
void fill_vector(const int *tab1, const int *tab2, int size, vector<Point> *vec);

// funkcje warunku przejscia dla funkcji sort
bool sort_x(Point a, Point b) { return a.x < b.x; }

bool sort_y(Point a, Point b) { return a.y < b.y; }

// funkcja obliczajaca odleglosc miedzy punktami
double distance(Point a, Point b) { return pow(b.x - a.x, 2) + pow(b.y - a.y, 2); }

// funkcja porownujaca punkty ze soba (kazdy z kazdym)
Segment compare_points(vector<Point> tab);

// funkcja szukajaca najblizszych punktow
Segment nearest_points(vector<Point> tabX, vector<Point> tabY);

// funkcja losujaca wspolrzedne punktow w zakresie od 1 do 1000
void draw_points(int *tab, int n);

int main(void) {

    vector<Point> points;
    vector<Point> setX;
    vector<Point> setY;
    Segment result;

    srand((unsigned)time(NULL));

    // przypadek testowy 1: kilka losowo rozmieszczonych punktow
    //int tab_x[] = {1,1,3,5,9,7,7};
    //int tab_y[] = {1,2,4,5,7,7,3};

    // przypadek testowy 2: wiele losowych wartosci
    int n = 1000000;
    int *tab_x = new int[n]; draw_points(tab_x,n);
    int *tab_y = new int[n]; draw_points(tab_y,n);

    int size = sizeof(tab_x)/sizeof(tab_x[0]);

    // wypelnienie wektora vec na podstawie zadanych tablic tab_x i tab_y
    fill_vector(tab_x, tab_y, size, &points);

    // przepisanie zawartosci wektora points
    setX = points;
    setY = points;

    // sortowanie vecX i vecY wedlug zadanych wspolrzednych (odpowiednio X i Y)
    sort(setX.begin(), setX.end(), sort_x);
    sort(setY.begin(), setY.end(), sort_y);

    result = nearest_points(setX, setY);

    cout<<"Najblizej lezace punkty: ";
    cout<<"("<<result.a.x<<","<<result.a.y<<") - ("<<result.b.x<<","<<result.b.y<<")"<<endl;
    cout<<"Odleglosc miedzy nimi wynosi: "<<sqrt(distance(result.a, result.b))<<endl;

    return 0;
}

void fill_vector(const int *tab1, const int *tab2, int size, vector<Point> *vec) {
    Point temp = {0,0};

    for(int i=0; i<size; i++) {
        temp.x = tab1[i];
        temp.y = tab2[i];

        vec->push_back(temp);
    }
}

Segment compare_points(vector<Point> tab) {
    int size = tab.size();
    double min = INFINITY;

    Segment result = {{0,0},{0,0}};
    Segment temp = {{0,0},{0,0}};

    for(int i=0; i<size-1; i++) {
        for(int j=i+1; j<size; j++) {
            temp.a = tab[i];
            temp.b = tab[j];
            if(distance(temp.a, temp.b) < min) {
                min = distance(temp.a, temp.b);
                result = temp;
                result.dist = min;
            }
        }
    }

    return result;
}

Segment nearest_points(vector<Point> tabX, vector<Point> tabY) {
    int size = tabX.size();
    int mid = size / 2;

    Segment result = {{0,0},{0,0}};
    Segment temp = {{0,0},{0,0}};

    if(size <= 3){
        // najmniejszy mozliwy podzial (porownywanie punktow)
        return compare_points(tabX);
    } else {
        // wyznaczanie zbiorow posortowanych po X dla lewego i prawego obszaru
        vector<Point> tabX_left (tabX.begin(), tabX.begin() + mid);
        vector<Point> tabX_right (tabX.begin() + mid , tabX.end());

        // wyznaczanie zbiorow posortowanych po Y dla lewego i prawego obszaru
        vector<Point> tabY_left;
        vector<Point> tabY_right;

        for(int i=0; i<size; i++) {
            if(tabY[i].x <= tabX[mid].x) {
                tabY_left.push_back(tabY[i]);
            } else {
                tabY_right.push_back(tabY[i]);
            }
        }

        // rekurencyjne poszukiwanie najblizszych punktow
        Segment left_seg = nearest_points(tabX_left, tabY_left);
        Segment right_seg = nearest_points(tabX_right, tabY_right);

        // wyznaczenie najkrotszego dystansu w lewym i prawym obszarze i wybor minimum
        double dist_left = distance(left_seg.a, left_seg.b);
        double dist_right = distance(right_seg.a, right_seg.b);

        double dist = min(dist_left, dist_right);

        // skrocenie rozmiaru Y
        vector<Point> lineY;

        for(int i=0; i<size; i++) {
            if((tabY[i].x > tabX[mid].x-dist) && (tabY[i].x < tabX[mid].x+dist)) {
                lineY.push_back(tabY[i]);
            }
        }

        // sprawdzenie minimum w okolicy linii przeciecia
        for(int i=0; i<size-1; i++) {
            for (int j = i+1; j<size && (lineY[j].y - lineY[i].y) < dist; j++) {
                temp.a = lineY[i];
                temp.b = lineY[j];
                if (distance(lineY[i], lineY[j]) < dist) {
                    result = temp;
                    result.dist = distance(lineY[i], lineY[j]);
                }
            }
        }

        // zwrocenie wyniku
        if(dist == dist_left) {

            if(result.dist >= dist) {
                return result;
            } else {
                return left_seg;
            }

        } else if(dist == dist_right) {

            if(result.dist >= dist) {
                return result;
            } else {
                return right_seg;
            }

        }
        return result;
    }
}

void draw_points(int *tab, int n){
    for(int i=0; i<n; i++){
        tab[i] = (rand()%1000)+1;
    }
}
