#include <iostream>
#include <cmath>
#include <bitset>

// Jackiewicz Łukasz
// Geometria, grafy, algorytmy
// Zadanie 6: Problem komiwojażera
//
// Kompilacja: g++ main.cpp
// Uruchomienie: ./a.out lub main.exe

using namespace std;

// funkcja wyznaczajaca minimalny cykl Hamiltona w danym grafie
// input:   position - pozycja startowa (id miasta)
//          visited - zapis binarny odwiedzonych miast (bitmask),
//                  np. 0101 (DCBA) -> odwiedzone miasta C i A
int tsp(int n, int cities[4][4], int distances[4][16], int position, int visited);

int main(void) {

    int n = 4;
    // odległości między miastami: Kutno, Warszawa, Poznań Kraków
    int cities[4][4] = {
            {0,130,180,300},
            {130,0,320,350},
            {180,320,0,360},
            {300,350,360,0}
    };

    int distances[4][16];

    for(int i=0; i<n; i++) {
        for(int j=0; j<(1<<n); j++) {
            distances[i][j] = -1;
        }
    }

    cout<<"Problem komiwojażera: wyznaczanie najkrotszej trasy między miastami (Kutno, Warszawa, Poznań, Kraków)"<<endl;
    cout<<"Najkrotszy dystans: "<<tsp(n, cities, distances, 0, 1)<<endl;

    return 0;
}

int tsp(int n, int cities[4][4], int distances[4][16], int position, int visited) {

    int result = INFINITY;

    // czy wszystkie miasta zostały odwiedzone
    if(visited == ((1<<n)-1)) {
        return cities[position][0];
    }

    // optymalizacja: zwrócenie wcześniej już obliczonej wartości
    if(distances[position][visited]!=-1) {
        return distances[position][visited];
    }

    // przejście przez wszystkie możliwe miasta i wyznaczenie wyniku
    // i - id miasta
    for(int i=0; i<n; i++) {
        // sprawdzanie tylko tych miast, które nie zostały jeszcze odwiedzone
        if((visited & (1<<i)) == 0) {
            int actual_result = cities[position][i] + tsp(n, cities, distances, i, visited | (1<<i));
            result = min(result, actual_result);
        }
    }

    // zapisanie wyniku w macierzy wyznaczonych odległości
    distances[position][visited] = result;
    return result;
}