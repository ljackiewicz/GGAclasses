#include <iostream>
#include <algorithm>
#include <vector>

// Jackiewicz Lukasz
// Geometria, grafy, algorytmy
// Zadanie 3: kD-drzewa. Przeszukiwanie prostokatnych obszarow
//
// Kompilacja: g++ main.cpp
// Uruchomienie: ./a.out lub a.exe

using namespace std;

struct Point {
    int x, y;
};

struct Tree {
    vector<Point> points;
    Point division;
    Tree *left, *right;
};

struct Area {
    int minX, maxX;
    int minY, maxY;
};

// wyswietlanie zawartosci wektora
void print_points(vector<Point> tab);

// wypelnianie wektora podanymi tablicami
void fill_vector(const int *tab1, const int *tab2, int size, vector<Point> *vec);

// wypelnianie wspolrzednych obszaru do przeszukiwania
Area fill_area(int x_min, int x_max, int y_min, int y_max);

// warunki sortowania punktow
bool sort_x(Point a, Point b) { return a.x < b.x; }
bool sort_y(Point a, Point b) { return a.y < b.y; }

// budowanie drzewa
Tree* build_tree(vector<Point> tab, int height);

vector<Point> search_area(Tree *tree, Area area) {

    vector<Point> result;
    vector<Point> temp;

    if(tree->left == NULL && tree->right == NULL) {
        // czy punkt zawarty w obszarze
        if(tree->division.x >= area.minX && tree->division.x <= area.maxX && tree->division.y >= area.minY && tree->division.y <= area.maxY) {
            result.push_back(tree->division);
        }
    } else if(tree->left != NULL) {
        // sprawdzenie czy badany obszar nie wychodzi poza rozmieszczone punkty...

        temp = search_area(tree->left, area);
        result.insert(temp.end(), temp.begin(), temp.end());
    } else if(tree->right != NULL) {
        // sprawdzenie czy badany obszar nie wychodzi poza rozmieszczone punkty...

        temp = search_area(tree->right, area);
        result.insert(temp.end(), temp.begin(), temp.end());
    }

    return result;
}

int main(void) {
    vector<Point> points;

    //int tab_x[] = {1,2,3,5,4,7,8,7,6};
    //int tab_y[] = {2,5,9,1,3,9,6,4,7};
    int tab_x[] = {4,2,1};
    int tab_y[] = {3,5,2};

    int size = sizeof(tab_x)/sizeof(tab_x[0]);
    fill_vector(tab_x, tab_y, size, &points);

    Tree *kDtree;
    //Area searchArea = fill_area(6,8,3,5);
    Area searchArea = fill_area(3,5,2,4);

    build_tree(points, 0);
    print_points(search_area(kDtree, searchArea));

    return 0;
}

void print_points(vector<Point> tab) {
    for(int i=0; i<tab.size(); i++) {
        cout<<"("<<tab[i].x<<","<<tab[i].y<<")\t";
    }
    cout<<endl;
}

void fill_vector(const int *tab1, const int *tab2, int size, vector<Point> *vec) {
    Point temp = {0,0};

    for(int i=0; i<size; i++) {
        temp.x = tab1[i];
        temp.y = tab2[i];

        vec->push_back(temp);
    }
}

Area fill_area(int x_min, int x_max, int y_min, int y_max) {
    Area tempArea;

    tempArea.minX = x_min;
    tempArea.maxX = x_max;
    tempArea.minY = y_min;
    tempArea.maxY = y_max;

    return tempArea;
}

Tree* build_tree(vector<Point> tab, int height) {
    Tree *result = new Tree;
    result->points = tab;
    int size = tab.size();
    int half = size/2;

    vector<Point> tabX, tabY;
    tabX = tabY = tab;

    //wyswietlanie budowanego drzewa
    //print_points(tab);

    if(size == 1) {
        // jedyny element w poddrzewie (lisc)
        result->division = tab[0];
        result->left = NULL;
        result->right = NULL;
        return result;
    } else if(height%2 == 0) {
        // wysokosc drzewa parzysta - sortowanie po X
        sort(tabX.begin(), tabX.end(), sort_x);
        result->division = tabX[half];

        // podział przestrzeni na lewa i prawa
        vector<Point> leftX (tabX.begin(), tabX.begin()+half);
        vector<Point> rightX (tabX.begin()+half, tabX.end());

        // budowanie lewego i prawego poddrzewa
        result->left = build_tree(leftX, height++);
        result->right = build_tree(rightX, height++);
        return result;
    } else {
        // wysokosc drzewa nieparzysta - sortowanie po Y
        sort(tabY.begin(), tabY.end(), sort_y);
        result->division = tabY[half];

        // podział przestrzeni na lewa i prawa
        vector<Point> leftY (tabY.begin(), tabY.begin()+half);
        vector<Point> rightY (tabY.begin()+half, tabY.end());

        // budowanie lewego i prawego poddrzewa
        result->left = build_tree(leftY, height++);
        result->right = build_tree(rightY, height++);
        return result;
    }
}