#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

// Jackiewicz Lukasz
// nr albumu: 222888
//
// Geometria, grafy, algorytmy
// Zadanie 1: {0}-jadro wielokata prostego
//
// Program sprawdza czy zadany wielokat prosty posiada {0}-jadro

using namespace std;

void rozszerz_tab(vector<int> *X, vector<int> *Y);

void wyznacz_min_max(vector<int> axisX, vector<int> axisY, vector<int> *min, vector<int> *max);

void sprawdz_jadro(vector<int> min, vector<int> max);

int main(void) {
	
	// przypadek testowy 1: istnieja min i max lokalne
	int axisX[] = {1,1,3,2,2,3,3,2,4,5,7,12,14,14,12,12,10,9,6,5};
	int axisY[] = {13,11,9,8,6,5,4,3,1,1,4,4,3,9,7,9,9,11,11,13};
	
	// przypadek testowy 2: brak jadra
	//int axisX[] = {1,1,2,4,6,3,2};
	//int axisY[] = {7,4,1,5,4,7,3};
	
	vector<int> vecX (axisX, axisX + sizeof(axisX) / sizeof(int) );
	vector<int> vecY (axisY, axisY + sizeof(axisY) / sizeof(int) );
	
	vector<int> min;
	vector<int> max;
	
	if(vecX.size() == vecY.size()) {
		cout<<"Ilosc wierzcholkow wielokata: "<<vecX.size()<<endl<<endl;
	} else {
		cout<<"Tablice zawierajace wspolrzedne wierzcholkow nie sa sobie rowne!"<<endl<<endl;
	}
	
	// rozszerzanie tablic o dwa poczatkowe wierzcholki
	rozszerz_tab(&vecX, &vecY);
	
	// wyznaczanie tablic min i max zawierajace minima i maksima lokalne
	wyznacz_min_max(vecX, vecY, &min, &max);
	
	// sprawdzanie czy {0}-jadro wielokata nie jest puste
	sprawdz_jadro(min, max);
	
	return 0;
}

void rozszerz_tab(vector<int> *X, vector<int> *Y){
	// rozszerzenie tablic X i Y od dwa pierwsze elementy
	
	X->push_back((*X)[0]);
	X->push_back((*X)[1]);
	
	Y->push_back((*Y)[0]);
	Y->push_back((*Y)[1]);
}

void sprawdz_jadro(vector<int> min, vector<int> max){
	int minElem = (int)*min_element(min.begin(), min.end());
	int maxElem = (int)*max_element(max.begin(), max.end());
	
	if(maxElem <= minElem) {
		cout<<"min: "<<minElem<<"\t"<<"max: "<<maxElem<<endl;
		cout<<"Jadro tego wielokata nie jest puste"<<endl;
	} else {
		cout<<"min: "<<minElem<<"\t"<<"max: "<<maxElem<<endl;
		cout<<"Jadro tego wielokata jest puste"<<endl;
	}
}

void wyznacz_min_max(vector<int> axisX, vector<int> axisY, vector<int> *min, vector<int> *max){
	// wyznaczanie minimow i maksimow lokalnych
	for(int i=1; i<axisX.size()-1; i++) {
		
		if(axisY[i-1] < axisY[i]) {
			// 1: przejscie w gore
			
			if(axisY[i] < axisY[i+1]) {
				// 2: przejscie w gore
				// nic - 2 x gora
			} else if(axisY[i] > axisY[i+1]) {
				// 2: przejscie w dol
				if(!(axisX[i+1] < axisX[i] || axisX[i] < axisX[i-1])){
					// maksimum lokalne
					max->push_back(axisY[i]);
				}
			} else {
				// 2: przejscie na rowni
				if(axisY[i+1] > axisY[i+2]) {
					// 3: przejscie w dol
					if(!(axisX[i+1] < axisX[i] || axisX[i] < axisX[i-1])){
						// maksimum lokalne
						max->push_back(axisY[i]);
					}
				}
			}
			
		} else if(axisY[i-1] > axisY[i]) {
			// 1: przejscie w dol
			
			if(axisY[i] < axisY[i+1]) {
				// 2: przejscie w gore
				if(!(axisX[i+1] > axisX[i] || axisX[i] > axisX[i-1])){
					// minimum lokalne
					min->push_back(axisY[i]);
				}
			} else if(axisY[i] > axisY[i+1]) {
				// 2: przejscie w dol
				// nic - 2 x dol
			} else {
				// 2: przejscie na rowni
				if(axisY[i+1] < axisY[i+2]) {
					// 3: przejscie w gore
					if(!(axisX[i+1] > axisX[i] || axisX[i] > axisX[i-1])){
						// minimum lokalne
						min->push_back(axisY[i]);
					}
				}
			}
		}
	}
	
	// jesli nie istnieje min/max lokalne to pod min/max wstawiana jest skrajny wierzcholek wielokata (najwyzszy/najnizszy)
	if(min->size()==0) {
		min->push_back((int)*max_element(axisY.begin(), axisY.end()));
	}
	
	if(max->size()==0) {
		max->push_back((int)*min_element(axisY.begin(), axisY.end()));
	}
}
