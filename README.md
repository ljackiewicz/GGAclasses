### Geometria, grafy, algorytmy

**Ćwiczenia 1** : 22.02.2018 </br>
- Zadanie: Program wyznaczający {0&deg;}-jądro wielokąta prostego (5 pkt)</br>
- Rozwiązanie zadania należy wysłać do dn. 8.III.2018, godz. 3:59:59</br>
</br>

---

**Ćwiczenia 2** : 08.03.2018 </br>
- Zadanie: Para najbliższych punktów wyznaczona metodą dziel i zwyciężaj (5 pkt)</br>
- Rozwiazanie zadania należy wysłać do dn. 22.III.2018, godz. 3:59:59</br>
</br>

---

**Ćwiczenia 3** : 22.03.2018 </br>
- Zadanie: kD-drzewa na płaszczyźnie: budowanie + przeszukiwanie + porównanie* (3+2+1* pkt)</br>
- Rozwiązanie zadania należy wysłać do dn. 12.IV.2018, godz. 3:59:59</br>
</br>

---

**Ćwiczenia 4** : 12.04.2018 </br>
- Zadania do wyboru: 
  - Wyznaczanie otoczki wypukłej (skan Grahama) (3 pkt)</br>
  - Wyznaczanie punktów przecięć poziomych i pionowych odcinków (4 pkt)</br>
  - Triangulacja wielokąta ściśle monotonicznego (5 pkt)</br>
- Rozwiązanie zadania należy wysłać do dn. 26.IV.2018, godz. 3:59:59</br>
</br>

---

**Ćwiczenia 5** : 26.04.2018 </br>
- Zadania do wyboru:
  - Prostokątna gałąź Steinera (5 pkt)</br>
  - Ochrona fortec (5 pkt)</br>
- Rozwiązanie zadania należy wysłać do dn. 10.V.2018, godz. 3:59:59</br>
</br>

---

**Ćwiczenia 6** : 10.05.2018 </br>
- Zadania do wyboru:
  - Największy zbiór niezależny w drzewach (4 pkt)</br>
  - Problem komiwojażera (5 pkt)</br>
  - Prostokątna gałąź Steinera (5 pkt)</br>
- Rozwiązanie zadania należy wysłać do dn. 24.V.2018, godz. 3:59:59</br>
</br>

---

**Ćwiczenia 7** : 24.05.2018 </br>
- Zadania do wyboru:
  - Problem najmniejszego rozcięcia (5 pkt)</br>
  - Problem długiej ścieżki w grafach skierowanych (algorytm RandomLongPath) (5 pkt)</br>
- Rozwiązanie zadania należy wysłać do dn. 7.VI.2018, godz. 3:59:59</br>
</br>
